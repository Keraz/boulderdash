package managers;

import contracts.ControllerInterface;
import contracts.ManagerInterface;
import contracts.ModelInterface;
import dao.SqlDao;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.Character;
import models.Map;
import models.MappableFactory;
import models.ModelType;
import models.superclasses.Mappable;

import javax.imageio.ImageIO;
import java.sql.*;
import java.util.ArrayList;

public class MapManager implements ManagerInterface {

    /**
     *
     * @param model the model to insert to the database
     */
    @Override
    public void insert(ModelInterface model)
    {
        try
        {
            Connection connection = SqlDao.getInstance().getConnection();

            if(model instanceof Map)
            {
                Map map = (Map) model;

                PreparedStatement mapRequest = connection.prepareStatement("INSERT INTO levels VALUES (?, ?, ?, ?);");
                mapRequest.setInt(1, map.getId());
                mapRequest.setInt(2, map.getTime());
                mapRequest.setInt(3, map.getWidth());
                mapRequest.setInt(4, map.getHeight());
                mapRequest.execute();

                map.getMap().forEach((position, element) ->
                {
                    try
                    {
                        PreparedStatement elementRequest = connection.prepareStatement("INSERT INTO levels_sprites VALUES (?, ?, ?, ?);");
                        elementRequest.setInt(1, map.getId());
                        elementRequest.setInt(2, ModelType.getType(element.getClass().getSimpleName()).getId());
                        elementRequest.setInt(3, position.getX());
                        elementRequest.setInt(4, position.getY());
                        elementRequest.execute();
                    }
                    catch(SQLException e)
                    {
                        e.printStackTrace();
                    }
                });
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param model The model to update in the database
     */
    @Override
    public void update(ModelInterface model)
    {
        delete(model);
        insert(model);
    }

    /**
     *
     * @param model The model to delete from the database
     */
    @Override
    public void delete(ModelInterface model)
    {
        try
        {
            Connection connection = SqlDao.getInstance().getConnection();

            if(model instanceof Map)
            {
                Map map = (Map) model;

                PreparedStatement elementsRequest = connection.prepareStatement("DELETE FROM levels_sprites WHERE id_level = ?;");
                elementsRequest.setInt(1, map.getId());
                elementsRequest.execute();

                PreparedStatement mapRequest = connection.prepareStatement("DELETE FROM levels WHERE id_level = ?;");
                mapRequest.setInt(1, map.getId());
                mapRequest.execute();
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<ModelInterface> selectAll()
    {
        return null; //TODO: NOT IMPLEMENTED YET
    }

    /**
     *
     * @param id The id in the database of the model you're looking for
     * @return The model that matches with the ID you've given
     */
    @Override
    public ModelInterface select(int id)
    {
        Connection connection = SqlDao.getInstance().getConnection();
        try {
            String sql = "{call levelselection(?)}";
            PreparedStatement gameRequest = connection.prepareCall(sql);
            gameRequest.setInt(1, id);
            ResultSet rs = gameRequest.executeQuery();
            Map map = null;
            while(rs.next()){
                map = new Map(rs.getInt(1));
                map.setTime(rs.getInt(2));
                map.setWidth(rs.getInt(3));
                map.setHeight(rs.getInt(4));
            }

            if(map == null)
                throw new NullPointerException("The map hasn't been found.");

            sql = "{call spriteposition(?)}";
            PreparedStatement contentRequest = connection.prepareCall(sql);
            contentRequest.setInt(1, id);
            rs = contentRequest.executeQuery();
            while(rs.next()){
                int x = rs.getInt(4);
                int y = rs.getInt(5);
                Sprite sprite = new Sprite(rs.getString(2));
                Mappable element = new MappableFactory().createModel(map, rs.getString(1), x, y, sprite);
                map.putElement(new Position(x, y), element);

                if(element instanceof Character)
                {
                    map.setPlayer((Character) element);
                }
            }

            return map;

        }
        catch(SQLException | NullPointerException e){
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    /**
     * count is counting the number of levels
     * @return columnindex 1
     */
    @Override
    public int count()
    {
        try
        {
            Connection connection = SqlDao.getInstance().getConnection();
            PreparedStatement request = connection.prepareCall("CALL count_levels()");
            ResultSet rs = request.executeQuery();
            while(rs.next())
            {
                return rs.getInt(1);
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

        return 0;
    }
}
