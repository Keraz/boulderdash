package managers;

import contracts.ManagerInterface;
import contracts.ModelInterface;
import dao.SqlDao;
import lib.graphics.Sprite;
import models.Map;
import models.MappableFactory;
import models.superclasses.Mappable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SpriteManager implements ManagerInterface {
    /**
     *
     * @param model the model to insert to the database
     */
    @Deprecated
    @Override
    public void insert(ModelInterface model)
    {

    }

    /**
     *
     * @param model The model to update in the database
     */
    @Deprecated
    @Override
    public void update(ModelInterface model)
    {

    }

    @Deprecated
    @Override
    public void delete(ModelInterface model)
    {

    }

    /**
     *
     * @param id The id in the database of the model you're looking for
     * @return null
     */
    @Override
    public ModelInterface select(int id)
    {
        return null;
    }

    /**
     * get all the sprites
     * @return elements
     */
    @Override
    public ArrayList<ModelInterface> selectAll()
    {
        try
        {
            Map map = new Map(0);
            ArrayList<ModelInterface> elements = new ArrayList<>();

            Connection connection = SqlDao.getInstance().getConnection();
            PreparedStatement request = connection.prepareCall("CALL select_all_sprites()");
            ResultSet rs = request.executeQuery();
            while(rs.next())
            {
                elements.add(new MappableFactory().createModel(map, rs.getString(2), 0, 0, new Sprite(rs.getString(3))));
            }

            return elements;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Deprecated
    @Override
    public int count()
    {
        return 0;
    }
}
