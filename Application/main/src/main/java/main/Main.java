/**
 * @author Jean-Aymeric DIET jadiet@cesi.fr
 * @version 1.0
 */
package main;

import controllers.CoreController;

public abstract class Main {

    public static void main(final String[] args)
    {
        new CoreController().launchStarting();
    }
}
