package controllers.tests;

import controllers.EditorController;
import org.junit.Before;
import org.junit.Test;
import routing.Request;

import static org.junit.Assert.*;

public class EditorControllerTest {

    private EditorController controller;

    @Before
    public void setUp() throws Exception
    {
        this.controller = new EditorController();
    }

    /**
     * Test if a bad request throws an error.
     */
    @Test
    public void launchSaving()
    {
        try
        {
            controller.launchSaving(Request.MAIN_MENU);
            fail();
        }
        catch(IllegalArgumentException e)
        {
            assert e.getMessage().equals("The request argument is not valid. Must be of type Map.");
        }
    }
}