package controllers.tests;

import controllers.CoreController;
import org.junit.Before;
import org.junit.Test;
import routing.Request;

import static org.junit.Assert.*;

public class CoreControllerTest {

    private CoreController controller;

    @Before
    public void setUp() throws Exception
    {
        this.controller = new CoreController();
    }

    /**
     * Test if a bad request throws an error.
     */
    @Test
    public void launchMapChoiceMenu()
    {
        try
        {
            Request request = Request.CHOOSE_GAME;
            request.setArgument(5);
            controller.launchMapChoiceMenu(request);
            fail();
        }
        catch(IllegalArgumentException e)
        {
            assert e.getMessage().equals("The request argument is not valid. Must be of type int between 0 and 1.");
        }
    }
}