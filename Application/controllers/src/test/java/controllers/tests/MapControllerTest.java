package controllers.tests;

import controllers.MapController;
import models.Map;
import org.junit.Before;
import org.junit.Test;
import routing.Request;

import static org.junit.Assert.*;

public class MapControllerTest {

    private MapController controller;

    @Before
    public void setUp() throws Exception
    {
        this.controller = new MapController();
    }

    /**
     * Test if a bad request throws an error.
     */
    @Test
    public void launchVictory()
    {
        try
        {
            controller.launchVictory(Request.MAIN_MENU);
            fail();
        }
        catch(IllegalArgumentException e)
        {
            assert e.getMessage().equals("The request argument is not valid. Must be of type Map.");
        }
    }

    @Test
    public void launchDefeat()
    {
        try
        {
            controller.launchDefeat(Request.MAIN_MENU);
            fail();
        }
        catch(IllegalArgumentException e)
        {
            assert e.getMessage().equals("The request argument is not valid. Must be of type Map.");
        }
    }
}