package controllers.tests;

import controllers.CharacterController;
import lib.graphics.Sprite;
import models.Character;
import models.Map;
import org.junit.Before;
import org.junit.Test;
import routing.Request;

import static org.junit.Assert.*;

public class CharacterControllerTest {

    private CharacterController characterController;

    @Before
    public void setUp() throws Exception
    {
        this.characterController = new CharacterController(new Character(new Map(1), 0, 0, null));
    }

    /**
     * Test if a bad request throws an error.
     */
    @Test
    public void launchMoving()
    {
        try
        {
            Request request = Request.MAIN_MENU;
            this.characterController.launchMoving(request);
            fail();
        }
        catch(IllegalArgumentException e)
        {
            assert e.getMessage().equals("The request argument is not valid. Must be of type Direction.");
        }
    }
}