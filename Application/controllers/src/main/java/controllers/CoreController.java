package controllers;

import contracts.ControllerInterface;
import dao.SqlDao;
import managers.MapManager;
import routing.Request;
import views.MainMenuView;
import views.MapChoiceMenuView;

public class CoreController implements ControllerInterface {

    /**
     * Called when the application is starting
     */
    public void launchStarting()
    {
        SqlDao.createInstance();
        launchMainMenu(Request.LAUNCH_GAME);
    }

    /**
     * Order the main menu to show
     * @param request The request information given by the router
     */
    public void launchMainMenu(Request request)
    {
        new MainMenuView().display();
    }

    /**
     * Order the map choice menu to show
     * @param request The request information given by the router
     */
    public void launchMapChoiceMenu(Request request) throws IllegalArgumentException
    {
        int i = (int)request.getArgument();
        if(i == 0 || i == 1)
            new MapChoiceMenuView(i == 0, new MapManager().count()).display();
        else
            throw new IllegalArgumentException("The request argument is not valid. Must be of type int between 0 and 1.");
    }

}
