package controllers;

import contracts.ControllerInterface;
import lib.graphics.Direction;
import models.Character;
import routing.Request;

public class CharacterController implements ControllerInterface {

    /**
     * The character controlled by the player
     */
    private Character player;

    /**
     *
     * @param character The character controlled by the player
     */
    public CharacterController(Character character)
    {
        this.player = character;
    }

    /**
     * Order the character to move
     * @param request The request information given by the router
     */
    public void launchMoving(Request request) throws IllegalArgumentException
    {
        if(request.getArgument() instanceof Direction)
        {
            Direction direction = (Direction) request.getArgument();

            if(player.canMove(direction) && !player.isMoving())
            {
                player.move(direction);
            }
        }
        else
        {
            throw new IllegalArgumentException("The request argument is not valid. Must be of type Direction.");
        }
    }
}
