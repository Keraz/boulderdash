package controllers;

import contracts.ControllerInterface;
import managers.MapManager;
import managers.SpriteManager;
import models.Map;
import routing.Request;
import views.EditorView;
import views.MapView;

public class EditorController implements ControllerInterface {

    /**
     * Called when we want to use the map editor
     * @param request The request information given by the router
     */
    public void launchEditor(Request request)
    {
        MapManager manager = new MapManager();
        int levelsCount = manager.count();
        int level = (int) request.getArgument();
        Map map = null;
        if(level > levelsCount)
        {
            map = new Map(levelsCount + 1);
        }
        else
        {
            map = (Map) manager.select(level);
        }

        MapView editorView = new EditorView(map, new SpriteManager().selectAll());
        editorView.display();
        map.addObserver(editorView);
    }

    /**
     * Saves the edited map into the database
     * @param request The request information given by the router
     */
    public void launchSaving(Request request) throws IllegalArgumentException
    {
        if(request.getArgument() instanceof Map)
        {
            Map map = (Map) request.getArgument();
            MapManager manager = new MapManager();
            int levelsCount = manager.count();

            if(map.getId() > levelsCount)
                manager.insert(map);
            else
                manager.update(map);

            new CoreController().launchMainMenu(Request.MAIN_MENU);
        }
        else
        {
            throw new IllegalArgumentException("The request argument is not valid. Must be of type Map.");
        }
    }

}
