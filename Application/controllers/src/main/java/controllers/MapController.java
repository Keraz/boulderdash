package controllers;

import contracts.ControllerInterface;
import managers.MapManager;
import models.Map;
import routing.Request;
import routing.Router;
import views.EndGameView;
import views.MapView;

public class MapController implements ControllerInterface {

    /**
     * Called when a game is starting
     * @param request The request information given by the router
     */
    public void launchMap(Request request)
    {
        MapManager manager = new MapManager();
        Map map = (Map) manager.select((int) request.getArgument());
        Router.addController(CharacterController.class, new CharacterController(map.getPlayer()));

        MapView mapView = new MapView(map);
        map.addObserver(mapView);
        mapView.display();
        map.init();
    }

    /**
     * Called when the player achieves victory
     * @param request The request information given by the router
     */
    public void launchVictory(Request request)
    {
        if(request.getArgument() instanceof Map)
        {
            Map map = (Map) request.getArgument();
            new EndGameView(map, true).display();
        }
        else
        {
            throw new IllegalArgumentException("The request argument is not valid. Must be of type Map.");
        }
    }

    /**
     * Called when the player loses
     * @param request The request information given by the router
     */
    public void launchDefeat(Request request)
    {
        if(request.getArgument() instanceof Map)
        {
            Map map = (Map) request.getArgument();
            new EndGameView(map, false).display();
        }
        else
        {
            throw new IllegalArgumentException("The request argument is not valid. Must be of type Map.");
        }
    }

}
