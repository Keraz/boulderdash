package contracts;

public interface ViewInterface
{
    /**
     * Shows the view in the screen.
     */
    void display();
}
