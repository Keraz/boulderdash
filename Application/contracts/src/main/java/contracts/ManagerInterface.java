package contracts;

import java.util.ArrayList;

public interface ManagerInterface {

    /**
     * Insert the given model into the database
     * @param model the model to insert to the database
     */
    void insert(ModelInterface model);

    /**
     * Update the given model database entry with the current parameters
     * @param model The model to update in the database
     */
    void update(ModelInterface model);

    /**
     * Delete the given model from the database
     * @param model The model to delete from the database
     */
    void delete(ModelInterface model);

    /**
     * Fetch a model into a database
     * @param id The id in the database of the model you're looking for
     */
    ModelInterface select(int id);

    /**
     * Select all the elements in the database
     * @return All the elements stored into the database
     */
    ArrayList<ModelInterface> selectAll();

    /**
     * Count all the elements in the database
     * @return The number of entries in the database
     */
    int count();

}
