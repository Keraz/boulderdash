package lib.graphics;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionTest {

    private Position position;

    @Before
    public void setUp() throws Exception
    {
        this.position = new Position(1, 1);
    }

    @Test
    public void compareTo()
    {
        assert position.compareTo(new Position(0, 0)) == -1;
        assert position.compareTo(new Position(2, 2)) == 1;
        assert position.compareTo(new Position(1, 1)) == 0;
    }

    @Test
    public void equals()
    {
        assert position.equals(new Position(1, 1));
        assert !position.equals(new Position(0, 0));
    }

}