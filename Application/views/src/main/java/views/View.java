package views;

import contracts.ViewInterface;
import lib.graphics.Window;
import lib.observer.Observer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.io.IOException;

public abstract class View implements ViewInterface, Observer, KeyListener {

    private Window window;

    /**
     * view for the window
     */
    public View()
    {
        this.window = Window.getInstance();
        this.window.removeAllKeyListeners();
        this.window.addKeyListener(this);
    }

    /**
     *
     * @return window
     */
    public Window getWindow()
    {
        return window;
    }

    /**
     *
     * @param window window
     */
    public void setWindow(Window window)
    {
        this.window = window;
    }

    /**
     *
     * @param text text
     * @return button
     */
    public JButton createButton(String text)
    {
        try
        {
            Image buttonImage = ImageIO.read(MenuView.class.getClassLoader().getResourceAsStream("background-button.png"));

            JButton button = new JButton(text, new ImageIcon(buttonImage));
            button.setBackground(new Color(0, 0, 0, 0));
            button.setBorder(BorderFactory.createEmptyBorder());
            button.setVerticalTextPosition(SwingConstants.CENTER);
            button.setHorizontalTextPosition(SwingConstants.CENTER);
            button.setFont(new Font("grobold", Font.PLAIN, 32));
            button.setForeground(Color.WHITE);

            return button;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
