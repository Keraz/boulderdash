package views;

import routing.Router;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class MapChoiceMenuView extends MenuView
{

    private int levelsCount;
    private boolean launchEditor;

    /**
     *
     * @param launchEditor launchEditor
     * @param levelsCount levelIsCount
     */
    public MapChoiceMenuView(boolean launchEditor, int levelsCount)
    {
        this.levelsCount = levelsCount;
        this.launchEditor = launchEditor;
    }

    /**
     * display on screen
     */
    @Override
    public void display()
    {
        try
        {
            Font font = Font.createFont(Font.TRUETYPE_FONT, MenuView.class.getClassLoader().getResourceAsStream("grobold.ttf"));
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
        }
        catch (IOException | FontFormatException e)
        {
            e.printStackTrace();
        }

        JPanel mainPanel = getWindow().getMainPanel();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();

        mainPanel.removeAll();

        int menuWidth = 300 * (launchEditor ? levelsCount + 1 : levelsCount);

        JPanel menu = new JPanel();
        menu.setLayout(new GridLayout(1, launchEditor ? levelsCount + 1 : levelsCount));
        menu.setBounds(
                (int)screenSize.getWidth() / 2 - menuWidth / 2,
                500,
                menuWidth,
                100
        );
        menu.setBackground(new Color(0, 0, 0, 0));
        mainPanel.add(menu);
        mainPanel.revalidate();
        mainPanel.repaint();

        for (int i = 1; i <= levelsCount; i++)
        {
            JButton button = createButton(String.valueOf(i));
            menu.add(button);
            menu.revalidate();

            final int levelID = i;

            button.addActionListener(e -> Router.createInstance(getClass()).callRoute(launchEditor ? -1 : -2, levelID));
        }

        if(launchEditor)
        {
            JButton newButton = createButton("+");
            menu.add(newButton);
            menu.revalidate();

            newButton.addActionListener(e -> Router.createInstance(getClass()).callRoute(-1, levelsCount + 1));
        }

        super.display();
    }

    /**
     * when the key is pressed
     * @param e e
     */
    @Override
    public void keyPressed(KeyEvent e)
    {
        System.out.println("KEY PRESSED: " + e.getKeyCode());
        Router.createInstance(getClass()).callRoute(e.getKeyCode());
    }

}
