package views;

import routing.Router;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class MainMenuView extends MenuView {
    /**
     * display on screen
     */
    @Override
    public void display()
    {
        try
        {
            JPanel mainPanel = getWindow().getMainPanel();
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();

            mainPanel.removeAll();

            JPanel menu = new JPanel();
            menu.setLayout(new BorderLayout());
            menu.setBounds(
                    (int)screenSize.getWidth() / 2 - 300,
                    500,
                    600,
                    100
            );
            menu.setBackground(new Color(0, 0, 0, 0));
            mainPanel.add(menu);
            mainPanel.revalidate();
            mainPanel.repaint();

            Image launchImage = ImageIO.read(MenuView.class.getClassLoader().getResourceAsStream("play-button.png"));
            JButton launchButton = new JButton(new ImageIcon(launchImage));
            launchButton.setBackground(new Color(0, 0, 0, 0));
            launchButton.setBorder(BorderFactory.createEmptyBorder());
            menu.add(launchButton, BorderLayout.WEST);
            menu.revalidate();

            launchButton.addActionListener(e -> Router.createInstance(getClass()).callRoute(KeyEvent.VK_NUMPAD0));

            Image editImage = ImageIO.read(MenuView.class.getClassLoader().getResourceAsStream("edit-button.png"));
            JButton editButton = new JButton(new ImageIcon(editImage));
            editButton.setBackground(new Color(0, 0, 0, 0));
            editButton.setBorder(BorderFactory.createEmptyBorder());
            menu.add(editButton, BorderLayout.EAST);
            menu.revalidate();

            editButton.addActionListener(e -> Router.createInstance(getClass()).callRoute(KeyEvent.VK_NUMPAD1));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        super.display();
    }

    /**
     * when the key is pressed
     * @param e e
     */
    @Override
    public void keyPressed(KeyEvent e)
    {
        System.out.println("KEY PRESSED: " + e.getKeyCode());
        Router.createInstance(getClass()).callRoute(e.getKeyCode());
    }


}
