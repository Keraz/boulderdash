package views;

import models.Map;
import routing.Route;
import routing.Router;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class EndGameView extends MenuView {

    private Map map;
    private boolean isVictory;

    /**
     *
     * @param map map
     * @param isVictory isVictory
     */
    public EndGameView(Map map, boolean isVictory)
    {
        this.map = map;
        this.isVictory = isVictory;
    }

    /**
     * display on screen
     */
    @Override
    public void display()
    {
        try
        {
            JPanel mainPanel = getWindow().getMainPanel();
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();

            mainPanel.removeAll();

            int menuWidth = 600;

            JPanel menu = new JPanel();
            menu.setLayout(new GridLayout(1, 2));
            menu.setBounds(
                    (int)screenSize.getWidth() / 2 - menuWidth / 2,
                    500,
                    menuWidth,
                    100
            );
            menu.setBackground(new Color(0, 0, 0, 0));
            mainPanel.add(menu);
            mainPanel.revalidate();
            mainPanel.repaint();

            JButton tryAgainButton = createButton("Try again");
            menu.add(tryAgainButton);
            menu.revalidate();
            menu.repaint();

            tryAgainButton.addActionListener(e -> Router.createInstance(getClass()).callRoute(-1, map.getId()));

            JButton mainMenuButton = createButton("Main menu");
            menu.add(mainMenuButton);
            menu.revalidate();
            menu.repaint();

            mainMenuButton.addActionListener(e -> Router.createInstance(getClass()).callRoute(-2));

            Image image = ImageIO.read(getClass().getClassLoader().getResourceAsStream(isVictory ? "victory_screen.png" : "defeat_screen.png"));
            JLabel background = new JLabel(new ImageIcon(image));
            background.setBounds(0, 0, (int) screenSize.getWidth(), (int) screenSize.getHeight());
            mainPanel.add(background);
            mainPanel.revalidate();
            mainPanel.repaint();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * the typeof the key
     * @param e e
     */
    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    /**
     * when the key is pressed
     * @param e e
     */
    @Override
    public void keyPressed(KeyEvent e)
    {

    }

    /**
     *  when the key is released
     * @param e e
     */
    @Override
    public void keyReleased(KeyEvent e)
    {

    }

    /**
     * update of object
     * @param object object
     */
    @Override
    public void update(Object object)
    {

    }
}
