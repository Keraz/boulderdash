package views;

import routing.Router;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public abstract class MenuView extends View
{
    /**
     * display on screen
     */
    @Override
    public void display()
    {
        try
        {
            JPanel mainPanel = getWindow().getMainPanel();
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();

            Image logoImage = ImageIO.read(MenuView.class.getClassLoader().getResourceAsStream("logo.png"));
            logoImage = logoImage.getScaledInstance((int)(logoImage.getWidth(null) / 1.5), (int)(logoImage.getHeight(null) / 1.5), Image.SCALE_SMOOTH);
            JLabel logo = new JLabel(new ImageIcon(logoImage));
            logo.setBounds(
                    (int)screenSize.getWidth() / 2 - logoImage.getWidth(null) / 2,
                    0,
                    logoImage.getWidth(null),
                    logoImage.getHeight(null)
            );
            mainPanel.add(logo);
            mainPanel.revalidate();
            mainPanel.repaint();

            Image backgroundImage = ImageIO.read(MenuView.class.getClassLoader().getResourceAsStream("menu-background.jpg"));
            backgroundImage = backgroundImage.getScaledInstance((int) screenSize.getWidth(), (int) screenSize.getHeight(), Image.SCALE_SMOOTH);
            JLabel background = new JLabel(new ImageIcon(backgroundImage));
            background.setBounds(
                    0,
                    0,
                    (int) screenSize.getWidth(),
                    (int) screenSize.getHeight()
            );
            mainPanel.add(background);
            mainPanel.revalidate();
            mainPanel.repaint();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * the type of the key
     * @param e e
     */
    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    /**
     * when the key is released
     * @param e e
     */
    @Override
    public void keyReleased(KeyEvent e)
    {

    }

    /**
     * update the object
     * @param object The object which has been sent by an observable
     */
    @Override
    public void update(Object object)
    {

    }
}
