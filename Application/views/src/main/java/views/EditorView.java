package views;

import contracts.ModelInterface;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.Map;
import models.MappableFactory;
import models.superclasses.Mappable;
import routing.Request;
import routing.Router;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;

public class EditorView extends MapView implements MouseMotionListener, MouseListener {

    private ArrayList<ModelInterface> elementsList;
    private Mappable selected;
    private Position position;
    private Mappable mouseOverElement;
    private JPanel emptyCaseOver;

    /**
     *
     * @param map map
     * @param elementsList elementList
     */
    public EditorView(Map map, ArrayList<ModelInterface> elementsList)
    {
        super(map);
        this.elementsList = elementsList;
        getWindow().addMouseMotionListener(this);
        getWindow().addMouseListener(this);
    }

    /**
     * display on screen
     */
    @Override
    public void display()
    {
        super.display();

        this.diamondsPanel.setVisible(false);
        JPanel mainPanel = getWindow().getMainPanel();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();

        JPanel toolbar = new JPanel();
        toolbar.setBounds((int) screenSize.getWidth() - 300, 0, 300, (int) screenSize.getHeight());
        toolbar.setBackground(new Color(25, 25, 25, 172));
        mainPanel.add(toolbar);
        mainPanel.revalidate();
        mainPanel.repaint();

        elementsList.forEach(model ->
        {
            Mappable mappable = (Mappable) model;
            Sprite sprite = mappable.getSprite();
            toolbar.add(sprite);
            toolbar.revalidate();
            toolbar.repaint();
        });

        try
        {
            JButton button = new JButton(new ImageIcon(ImageIO.read(getClass().getClassLoader().getResourceAsStream("save-button.png"))));
            button.setBackground(new Color(0, 0, 0, 0));
            button.setBorder(BorderFactory.createEmptyBorder());
            toolbar.add(button);
            toolbar.revalidate();
            toolbar.repaint();

            button.addActionListener(e -> Router.createInstance(getClass()).callRoute(-1, getMap()));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * check what is clicked
     * @param e e
     */
    @Override
    public void mouseClicked(MouseEvent e)
    {
        JPanel mainPanel = getWindow().getMainPanel();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();

        if(e.getX() < (int) screenSize.getWidth() - 300 && selected != null)
        {
            getMap().removeElement(position);
            mainPanel.remove(mouseOverElement == null ? emptyCaseOver : mouseOverElement.getSprite());

            if(e.getButton() == MouseEvent.BUTTON1)
            {
                Mappable newElement = new MappableFactory().createModel(getMap(), selected.getClass(), position.getX(), position.getY(), new Sprite(selected.getSprite().getImage()));
                getMap().putElement(position, newElement);

                mouseOverElement = newElement;
                newElement.getSprite().setBounds(position.getX() * 64, position.getY() * 64, 64, 64);
                mainPanel.add(newElement.getSprite());
            }
            else
            {
                mouseOverElement = null;

                if(position.getX() >= getMap().getWidth())
                    getMap().setHeight(position.getX() + 1);
                if(position.getY() >= getMap().getHeight())
                    getMap().setHeight(position.getY() + 1);
            }


            mainPanel.revalidate();
            mainPanel.repaint();
        }
        else
        {
            elementsList.forEach(model ->
            {
                Mappable mappable = (Mappable) model;
                Sprite sprite = mappable.getSprite();

                int targetMinX = (int) screenSize.getWidth() - 300 + sprite.getX();
                int targetMaxX = targetMinX + 64;
                int targetMinY = sprite.getY();
                int targetMaxY = targetMinY + 64;

                int mouseX = (int) e.getLocationOnScreen().getX();
                int mouseY = (int) e.getLocationOnScreen().getY() - 30;

                if
                (
                        (mouseX >= targetMinX &&
                                mouseX <= targetMaxX  &&
                                mouseY >= targetMinY  &&
                                mouseY <= targetMaxY)
                )
                {
                    this.selected = mappable;
                    System.out.println(mappable);
                }
            });
        }
    }

    /**
     * when the mouse is pressed
     * @param e e
     */
    @Override
    public void mousePressed(MouseEvent e)
    {

    }

    /**
     * when the mouse is released
     * @param e e
     */
    @Override
    public void mouseReleased(MouseEvent e)
    {

    }

    /**
     * when the mouse is entered
     * @param e e
     */
    @Override
    public void mouseEntered(MouseEvent e)
    {

    }

    /**
     * when the mouse is exited
     * @param e e
     */
    @Override
    public void mouseExited(MouseEvent e)
    {

    }

    /**
     * when the mouse is dragged
     * @param e e
     */
    @Override
    public void mouseDragged(MouseEvent e)
    {

    }

    /**
     * when the mouse is moving
     * @param e e
     */
    @Override
    public void mouseMoved(MouseEvent e)
    {
        JPanel mainPanel = getWindow().getMainPanel();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();

        if(mouseOverElement != null)
            mouseOverElement.getSprite().setBorder(BorderFactory.createEmptyBorder());
        else if(emptyCaseOver != null)
        {
            mainPanel.remove(emptyCaseOver);
            mainPanel.revalidate();
            mainPanel.repaint();
        }

        if(e.getX() < (int) screenSize.getWidth() - 300)
        {
            int x = e.getX() / 64;
            int y = (e.getY() - 30) / 64;
            this.position = new Position(x, y);

            this.mouseOverElement = getMap().getElement(new Position(x, y));

            if(mouseOverElement != null)
                mouseOverElement.getSprite().setBorder(BorderFactory.createLineBorder(Color.ORANGE));
            else
            {
                this.emptyCaseOver = new JPanel();
                emptyCaseOver.setBounds(x * 64, y * 64, 64, 64);
                emptyCaseOver.setBorder(BorderFactory.createLineBorder(Color.ORANGE));
                mainPanel.add(emptyCaseOver);
                mainPanel.revalidate();
                mainPanel.repaint();
            }
        }
    }
}
