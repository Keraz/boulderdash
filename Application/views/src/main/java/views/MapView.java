package views;

import lib.graphics.Sprite;
import models.Character;
import models.Map;
import models.superclasses.Mappable;
import models.superclasses.Movable;
import routing.Router;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class MapView extends View {

    private Map map;
    private JLabel background;
    protected JButton diamondsPanel;

    /**
     *
     * @param map map
     */
    public MapView(Map map)
    {
        this.map = map;
    }

    /**
     * display on screen
     */
    @Override
    public void display()
    {
            JPanel mainPanel = getWindow().getMainPanel();
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();
            mainPanel.removeAll();

            map.getMap().forEach((position, mappable) -> {
                Sprite sprite = mappable.getSprite();
                sprite.setBounds(position.getX() * 64, position.getY() * 64, 64, 64);
                mainPanel.add(sprite);
                mainPanel.repaint();
            });

        this.diamondsPanel = createButton(getMap().getDiamondsLeft() + " Diamonds");
        diamondsPanel.setBounds((int) screenSize.getWidth() - 300, 0, 300, 100);
        mainPanel.add(diamondsPanel);
        mainPanel.revalidate();
        mainPanel.repaint();
    }

    /**
     *
     * @param mappable mapplablr
     */
    public void redisplay(Mappable mappable)
    {

        JPanel mainPanel = getWindow().getMainPanel();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();

        diamondsPanel.setText(getMap().getDiamondsLeft() + " Diamonds");

        if((mappable instanceof Character && !mappable.isAlive()))
        {
            Router.createInstance(getClass()).callRoute(-3, getMap());
            return;
        }

        if(map.getDiamondsLeft() <= 0)
        {
            Router.createInstance(getClass()).callRoute(-2, getMap());
            return;
        }

        Thread thread = new Thread()
        {
            @Override
            public void run() {
                super.run();

                Sprite sprite = mappable.getSprite();
                mainPanel.remove(sprite);

                if(!mappable.isAlive())
                {
                    interrupt();
                    return;
                }

                if(mappable instanceof Movable)
                {
                    Movable movable = (Movable) mappable;
                    sprite.setBounds(
                            movable.getAbsoluteX(),
                            movable.getAbsoluteY(),
                            64,
                            64
                    );
                }

                mainPanel.add(sprite);
                mainPanel.revalidate();
                mainPanel.repaint();
                mainPanel.revalidate();
                getWindow().revalidate();

                this.interrupt();
            }
        };

        thread.start();
    }

    /**
     * update of object
     * @param object The object which has been sent by an observable
     */
    @Override
    public void update(Object object)
    {
        if(object instanceof Mappable)
            redisplay((Mappable) object);
        else
            display();
    }

    /**
     * the type of the key
     * @param e e
     */
    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    /**
     * when the key is pressed
     * @param e e
     */
    @Override
    public void keyPressed(KeyEvent e)
    {
        if(e.getKeyCode() == 27)
            getMap().getPlayer().remove();

        Router.createInstance(this.getClass()).callRoute(e.getKeyCode());
    }

    /**
     * when the key is released
     * @param e e
     */
    @Override
    public void keyReleased(KeyEvent e)
    {

    }

    /**
     * get the map
     * @return map
     */
    public Map getMap()
    {
        return map;
    }
}
