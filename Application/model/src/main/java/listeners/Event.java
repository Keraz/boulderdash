package listeners;

import listeners.Listener;

public class Event {

    Class<? extends Listener> listenerClass;

    /**
     *
     * @param listenerClass
     */
    public Event(Class<? extends Listener> listenerClass)
    {
        this.listenerClass = listenerClass;
    }

    /**
     *
     * @return the class listener
     */
    public Class<? extends Listener> getListenerClass()
    {
        return listenerClass;
    }

    /**
     *
     * @param listenerClass the listener of the class
     */
    public void setListenerClass(Class<? extends Listener> listenerClass)
    {
        this.listenerClass = listenerClass;
    }
}
