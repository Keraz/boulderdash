package listeners;

import lib.graphics.Direction;
import lib.graphics.Position;
import models.superclasses.Movable;

public class StartingMoveEvent extends MoveEvent {
    /**
     * Movement is starting
     * @param direction direction
     * @param sourceElement type of element
     * @param sourcePosition the position
     * @param targetPosition where the element wants to go
     */
    public StartingMoveEvent(Direction direction, Movable sourceElement, Position sourcePosition, Position targetPosition)
    {
        super(direction, sourceElement, sourcePosition, targetPosition);
    }

}
