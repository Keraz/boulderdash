package listeners;

import lib.graphics.Direction;
import lib.graphics.Position;
import models.superclasses.Mappable;
import models.superclasses.Movable;

public class MoveEvent extends Event {

    private static Class<? extends Listener> listenerClass = MoveListener.class;

    private Direction direction;
    private Movable  sourceElement;
    private Mappable targetElement;
    private Position sourcePosition;
    private Position targetPosition;

    /**
     * how to move
     * @param direction the direction
     * @param sourceElement type of element
     * @param sourcePosition the position
     * @param targetPosition where the element wants to go
     */
    public MoveEvent(Direction direction, Movable sourceElement, Position sourcePosition, Position targetPosition)
    {
        super(listenerClass);

        this.direction = direction;
        this.sourceElement = sourceElement;
        this.targetElement = sourceElement.getMap().getNextElement(sourceElement, direction);
        this.sourcePosition = sourcePosition;
        this.targetPosition = targetPosition;
    }

    /**
     *
     * @return direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     *
     * @return sourceElement
     */

    public Movable getSourceElement()
    {
        return sourceElement;
    }

    /**
     *
     * @return targetElement
     */
    public Mappable getTargetElement()
    {
        return targetElement;
    }

    /**
     *
     * @return sourcePosition
     */
    public Position getSourcePosition()
    {
        return sourcePosition;
    }

    /**
     *
     * @return targetPosition
     */
    public Position getTargetPosition()
    {
        return targetPosition;
    }
}
