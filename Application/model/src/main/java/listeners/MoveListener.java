package listeners;

public interface MoveListener extends Listener {

    void onStartingMove(StartingMoveEvent event);
    void onEndingMove(EndingMoveEvent event);
    void onMove(MoveEvent event);

}
