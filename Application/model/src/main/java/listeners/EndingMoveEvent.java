package listeners;

import lib.graphics.Direction;
import lib.graphics.Position;
import models.superclasses.Movable;

public class EndingMoveEvent extends MoveEvent {
    /**
     *
     * @param direction the direction of the character
     * @param sourceElement type of element
     * @param sourcePosition the position of the character
     * @param targetPosition where the element wants to go
     */
    public EndingMoveEvent(Direction direction, Movable sourceElement, Position sourcePosition, Position targetPosition)
    {
        super(direction, sourceElement, sourcePosition, targetPosition);
    }
}
