package models.superclasses;

import listeners.EndingMoveEvent;
import listeners.MoveEvent;
import listeners.StartingMoveEvent;
import lib.graphics.Direction;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.Character;
import models.Diamond;
import models.Enemy;
import models.Map;
import models.interfaces.*;

public class Movable extends Mappable {

    private int speed;
    private boolean isMoving;

    /**
     *
     * @param position The position of the model on the map
     * @param sprite The default sprite of the object
     * @param speed The number of pixels the object is moving through during 1 second
     */
    public Movable(Map map, Position position, Sprite sprite, int speed)
    {
        super(map, position, sprite);
        this.speed = speed;
    }

    /**
     *
     * @param x The x position of the model on the map
     * @param y The y position of the model on the map
     * @param sprite The default sprite of the object
     * @param speed The number of pixels the object is moving through during 1 second
     */
    public Movable(Map map, int x, int y, Sprite sprite, int speed)
    {
        super(map, x, y, sprite);
        this.speed = speed;
    }

    /**
     *
     * @param direction The direction you want the model to move toward
     */
    public boolean canMove(Direction direction)
    {
        Mappable element = getMap().getNextElement(this, direction);

        if(this instanceof Enemy)
        {
            return element == null || element instanceof Entity;
        }

        if(element instanceof Movable)
        {
            if(element instanceof Diamond)
                return this instanceof Character;

            if (element instanceof Pushable
                    && direction != Direction.BOTTOM
                    && direction != Direction.TOP
                    && (!((Movable) element).isMoving()))
                return ((Pushable) element).canPush(direction);

            if (element instanceof Attractable) {
                Position underElement = new Position(getGridX(), getGridY() + 2);
                return !(getMap().getElement(underElement) instanceof Solid);
            }
            if (element instanceof Entity)
                return true;

            return ((Movable) element).isMoving();
        }

        if(element == null || (element instanceof Walkable && this instanceof Character))
            return true;

        return false;
    }

    /**
     *
     * @param direction @param direction The direction you want the model to move toward
     */
    public void move(Direction direction)
    {
        setMoving(true);

        Position source = new Position(getGridPosition().getX(), getGridPosition().getY());

        Position target = new Position(getGridX() + direction.getxIncrement(), getGridY() + direction.getyIncrement());
        notifyListeners(new StartingMoveEvent(direction, this, source, target));

        Thread thread = new Thread()
        {
            @Override
            public void run() {
                super.run();

                while(isAlive() && !isInterrupted())
                {
                    int newX = (int)(getAbsoluteX() + (direction.getxIncrement() * 64 * speed * 0.033));
                    int newY = (int)(getAbsoluteY() + (direction.getyIncrement() * 64 * speed * 0.017));

                    boolean movePositionCondition = false;
                    boolean stopMovingCondition = false;

                    switch(direction)
                    {
                        case LEFT:
                            movePositionCondition = newX < getGridX() * 64 - 32;
                            stopMovingCondition = newX < target.getX() * 64;
                            break;
                        case RIGHT:
                            movePositionCondition = newX > getGridX() * 64 + 32;
                            stopMovingCondition = newX > target.getX() * 64;
                            break;
                        case TOP:
                            movePositionCondition = newY < getGridY() * 64 - 32;
                            stopMovingCondition = newY < target.getY() * 64;
                            break;
                        case BOTTOM:
                            movePositionCondition = newY > getGridY() * 64 + 32;
                            stopMovingCondition = newY > target.getY() * 64;
                    }

                    if(stopMovingCondition)
                    {
                        setAbsoluteX(target.getX() * 64);
                        setAbsoluteY(target.getY() * 64);

                        notifyListeners(new EndingMoveEvent(direction, Movable.this, source, target));

                        setMoving(false);

                        interrupt();
                        break;
                    }

                    setAbsoluteX(newX);
                    setAbsoluteY(newY);

                    if(movePositionCondition)
                    {
                        getMap().removeElement(getGridPosition());
                        setGridX(target.getX());
                        setGridY(target.getY());
                        getMap().putElement(getGridPosition(), Movable.this);
                    }

                    try {
                        sleep(17);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    notifyListeners(new MoveEvent(direction, Movable.this, source, target));
                }
            }
        };

        thread.start();
    }

    /**
     *
     * @return The speed of the model
     */
    public int getSpeed()
    {
        return speed;
    }

    /**
     *
     * @param speed The speed you want the model to move with
     */
    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }
}
//this comment is totally useless lmfaooooooo grgrgrgrggrgrgrgr xd i'm crying cuz its sooooooooo funny ahahahhahahaahhahahah ohohoho huuhuhuhuh ihihihihiih
//Incredible.