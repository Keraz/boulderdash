package models.superclasses;

import lib.graphics.Position;
import lib.graphics.Sprite;
import models.Map;
import models.interfaces.Collidable;
import models.interfaces.Destroyable;
import models.interfaces.Walkable;

public class Entity extends Movable implements Destroyable, Walkable
{
    /**
     *
     * @param map map
     * @param position position
     * @param sprite sprite
     * @param speed speed
     */
    public Entity(Map map, Position position, Sprite sprite, int speed)
    {
        super(map, position, sprite, speed);
    }

    /**
     *
     * @param map map
     * @param x x
     * @param y y
     * @param sprite sprite
     * @param speed speed
     */
    public Entity(Map map, int x, int y, Sprite sprite, int speed)
    {
        super(map, x, y, sprite, speed);
    }

    /**
     * remove the element
     */
    @Override
    public void remove()
    {
        this.setAlive(false);
    }
}
