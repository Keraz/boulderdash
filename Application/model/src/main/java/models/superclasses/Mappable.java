package models.superclasses;

import contracts.ModelInterface;
import listeners.Event;
import lib.graphics.Position;
import lib.graphics.Sprite;
import listeners.Listener;
import models.Map;
import models.interfaces.Collidable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;

public class Mappable implements ModelInterface, Collidable {

    private Map map;
    private Position gridPosition;
    private Position absolutePosition;
    private Sprite sprite;
    private boolean isAlive;
    private ArrayList<Listener> listeners;

    /**
     *
     * @param position The gridPosition of the model on the map
     * @param sprite The default sprite of the object
     */
    public Mappable(Map map, Position position, Sprite sprite)
    {
        this.map = map;
        this.gridPosition = position;
        this.absolutePosition = new Position(gridPosition.getX() * 64, gridPosition.getY() * 64);
        this.sprite = sprite;
        this.isAlive = true;
        listeners = new ArrayList<>();
    }

    /**
     *
     * @param x The x gridPosition of the model on the map
     * @param y The y gridPosition of the model on the map
     * @param sprite The default sprite of the object
     */
    public Mappable(Map map, int x, int y, Sprite sprite)
    {
        this.map = map;
        this.gridPosition = new Position(x, y);
        this.absolutePosition = new Position(gridPosition.getX() * 64, gridPosition.getY() * 64);
        this.sprite = sprite;
        this.isAlive = true;
        listeners = new ArrayList<>();
    }

    /**
     * check if element can move
     * @param object object
     * @return position
     */
    @Override
    public boolean isColliding(Collidable object)
    {
        if(!(object instanceof Mappable))
            return false;

        Mappable target = (Mappable) object;
        Mappable source = this;

        int targetMinX = target.getAbsoluteX();
        int targetMaxX = targetMinX + 64;
        int targetMinY = target.getAbsoluteY();
        int targetMaxY = targetMinY + 64;

        int sourceMinX = source.getAbsoluteX();
        int sourceMaxX = sourceMinX + 64;
        int sourceMinY = source.getAbsoluteY();
        int sourceMaxY = sourceMinY + 64;

        return (sourceMinX > targetMinX &&
                sourceMinX < targetMaxX &&
                sourceMinY >= targetMinY &&
                sourceMinY <= targetMaxY)
                ||
                (sourceMaxX > targetMinX &&
                sourceMaxX < targetMaxX &&
                sourceMaxY >= targetMinY &&
                sourceMaxY <= targetMaxY)
                ||
                (sourceMaxX >= targetMinX &&
                sourceMaxX <= targetMaxX &&
                sourceMinY > targetMinY &&
                sourceMinY < targetMaxY)
                ||
                (sourceMinX >= targetMinX &&
                sourceMinX <= targetMaxX &&
                sourceMaxY > targetMinY &&
                sourceMaxY < targetMaxY);

    }

    /**
     *
     * @param listener listener
     */
    public void addListener(Listener listener)
    {
        this.listeners.add(listener);
    }

    /**
     *
     * @param listener listener
     */
    public void removeListener(Listener listener)
    {
        this.listeners.remove(listener);
    }

    /**
     *
     * @param event event
     */
    public void notifyListeners(Event event)
    {
        listeners.forEach(listener ->
        {
            try
            {
                Class<? extends Listener> listenerClass = listener.getClass();

                if(event.getListenerClass().isInstance(listener))
                {
                    for (Method method : listenerClass.getMethods())
                    {
                        if(method.getParameterCount() == 1)
                        {
                            Parameter parameter = method.getParameters()[0];
                            Class<?> type = parameter.getType();

                            if(type.equals(event.getClass()))
                            {
                                method.invoke(listener, event);
                            }
                        }
                    }
                }
            }
            catch(IllegalAccessException | InvocationTargetException e)
            {
                e.printStackTrace();
            }
        });
    }

    /**
     *
     * @return The gridPosition of the object on the map
     */
    public Position getGridPosition() {
        return gridPosition;
    }

    /**
     *
     * @param gridPosition The gridPosition on the map you want the object to be on
     */
    public void setGridPosition(Position gridPosition) {
        this.gridPosition = gridPosition;
    }

    /**
     *
     * @return The current sprite of the object
     */
    public Sprite getSprite() {
        return sprite;
    }

    /**
     *
     * @param sprite The sprite you want the object to wear
     */
    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    /**
     *
     * @return map
     */
    public Map getMap() {
        return map;
    }

    /**
     *
     * @param map map
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     *
     * @return gridX
     */
    public int getGridX()
    {
        return this.gridPosition.getX();
    }

    /**
     *
     * @return GridY
     */
    public int getGridY()
    {
        return this.gridPosition.getY();
    }

    /**
     *
     * @param x x
     */
    public void setGridX(int x)
    {
        this.gridPosition.setX(x);
    }

    /**
     *
     * @param y y
     */
    public void setGridY(int y)
    {
        this.gridPosition.setY(y);
    }

    /**
     *
     * @return absolutePosition
     */
    public Position getAbsolutePosition() {
        return absolutePosition;
    }

    /**
     *
     * @param absolutePosition absolutePosition
     */
    public void setAbsolutePosition(Position absolutePosition) {
        this.absolutePosition = absolutePosition;
    }

    /**
     *
     * @return absoluteX
     */
    public int getAbsoluteX()
    {
        return absolutePosition.getX();
    }

    /**
     *
     * @param xPixel xPixel
     */
    public void setAbsoluteX(int xPixel)
    {
        this.absolutePosition.setX(xPixel);
    }

    /**
     *
     * @return absoluteY
     */
    public int getAbsoluteY()
    {
        return this.absolutePosition.getY();
    }

    /**
     *
     * @param yPixel yPixel
     */
    public void setAbsoluteY(int yPixel)
    {
        this.absolutePosition.setY(yPixel);
    }

    /**
     *
     * @return isAlive
     */
    public boolean isAlive() {
        return isAlive;
    }

    /**
     *
     * @param alive alive
     */
    public void setAlive(boolean alive)
    {
        isAlive = alive;
    }
}
