package models;

import lib.graphics.Position;
import lib.graphics.Sprite;
import models.interfaces.Collidable;
import models.interfaces.Destroyable;
import models.interfaces.Solid;
import models.superclasses.Mappable;

public class Wall extends Mappable implements Destroyable, Solid {

    private final static int speed = 0;

    /**
     *
     * @param map map
     * @param position position
     * @param sprite sprite
     */
    public Wall(Map map, Position position, Sprite sprite)
    {
        super(map, position, sprite);
    }

    /**
     *
     * @param map map
     * @param x x
     * @param y y
     * @param sprite sprite
     */
    public Wall(Map map, int x, int y, Sprite sprite)
    {
        super(map, x, y, sprite);
    }

    /**
     * remove the wall
     */
    @Override
    public void remove()
    {
        this.setAlive(false);
    }
}
