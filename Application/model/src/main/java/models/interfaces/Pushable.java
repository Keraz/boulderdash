package models.interfaces;

import lib.graphics.Direction;

public interface Pushable extends Collidable {

    void push(Direction direction);

    boolean canPush(Direction direction);

}
