package models.interfaces;

public interface Attractable extends Collidable {

    boolean canFall();
    void fall();

}
