package models.interfaces;

public interface Collidable {

    boolean isColliding(Collidable object);

}
