package models;

import lib.graphics.Direction;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.interfaces.*;
import models.superclasses.Entity;
import models.superclasses.Mappable;
import models.superclasses.Movable;

public class Rock extends Movable implements Destroyable, Attractable, Pushable, Solid {

    private final static int speed = 5;

    public Rock(Map map, Position position, Sprite sprite)
    {
        super(map, position, sprite, speed);
    }

    public Rock(Map map, int x, int y, Sprite sprite)
    {
        super(map, x, y, sprite, speed);
    }

    /**
     *
     * @return boolean
     */
    @Override
    public boolean canFall()
    {
        return canFallOnBlock(getMap().getNextElement(this, Direction.BOTTOM));
    }

    public boolean canFallOnBlock(Mappable support)
    {
        if(support == null)
            return true;

        if(support instanceof Rock)
            return false;

        if(!support.isAlive())
            return true;

        if(!( support instanceof Solid) && (!( support instanceof Entity) || isMoving()))
            return true;

        else if(support instanceof Movable)
            if(((Movable) support).isMoving())
                return true;

        return false;
    }

    /**
     * the rock fall
     */
    @Override
    public void fall()
    {
        move(Direction.BOTTOM);
    }

    /**
     * remove the rock
     */
    @Override
    public void remove()
    {
        this.setAlive(false);
    }

    /**
     * rock is pushed
     * @param direction direction
     */
    @Override
    public void push(Direction direction)
    {
        move(direction);
    }

    /**
     * when the rock can be pushed
     * @param direction direction
     * @return boolean
     */
    @Override
    public boolean canPush(Direction direction) {
        if (direction == Direction.BOTTOM || direction == Direction.TOP)
            return false;

        Mappable element = getMap().getNextElement(this, direction);

        return element == null;
    }
}
