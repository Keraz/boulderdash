package models;

import lib.graphics.Position;
import lib.graphics.Sprite;
import models.interfaces.Breakable;
import models.interfaces.Solid;
import models.interfaces.Walkable;
import models.superclasses.Mappable;

public class Dirt extends Mappable implements Breakable, Walkable, Solid {

    /**
     *
     * @param map map
     * @param position position
     * @param sprite sprite
     */
    public Dirt(Map map, Position position, Sprite sprite)
    {
        super(map, position, sprite);
    }

    /**
     *
     * @param map map
     * @param x x
     * @param y y
     * @param sprite sprite
     */
    public Dirt(Map map, int x, int y, Sprite sprite)
    {
        super(map, x, y, sprite);
    }

    /**
     * remove the dirt
     */
    @Override
    public void remove()
    {
        this.setAlive(false);
    }
}
