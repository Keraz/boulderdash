package models;

import lib.graphics.Direction;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.superclasses.Entity;

public class Enemy extends Entity {

    private static int speed = 3;

    /**
     *
     * @param map map
     * @param position position
     * @param sprite sprite
     */
    public Enemy(Map map, Position position, Sprite sprite)
    {
        super(map, position, sprite, speed);
    }

    /**
     *
     * @param map map
     * @param x x
     * @param y y
     * @param sprite sprite
     */
    public Enemy(Map map, int x, int y, Sprite sprite)
    {
        super(map, x, y, sprite, speed);
    }

    /**
     * direction of the enemy
     * @return direction
     */
    public Direction findPath()
    {
        Character player = getMap().getPlayer();
        Position characterPosition   = player.getGridPosition();
        Direction preferedDirectionX = player.getGridX() > this.getGridX() ? Direction.RIGHT  : Direction.LEFT;
        Direction preferedDirectionY = player.getGridY() > this.getGridY() ? Direction.BOTTOM : Direction.TOP;
        Direction preferedDirection  = Math.abs(player.getGridX() - this.getGridX()) > Math.abs(this.getGridY() - player.getGridY()) ? preferedDirectionX : preferedDirectionY;

        if(this.canMove(preferedDirection))
            return preferedDirection;
        else if(this.canMove(preferedDirection == preferedDirectionX ? preferedDirectionY : preferedDirectionX))
            return preferedDirection == preferedDirectionX ? preferedDirectionY : preferedDirectionX;
        else
        {
            for (Direction enemyDirection : Direction.values())
            {
                if(enemyDirection != preferedDirectionX && enemyDirection != preferedDirectionY)
                {
                    if(this.canMove(enemyDirection))
                        return enemyDirection;
                }
            }
        }

        return null;
    }

}
