package models;

import lib.graphics.Direction;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.interfaces.*;
import models.superclasses.Entity;
import models.superclasses.Mappable;
import models.superclasses.Movable;

public class Diamond extends Movable implements Attractable, Breakable, Walkable, Solid {

    private final static int speed = 5;

    /**
     *
     * @param position The position you want the character to be on
     */
    public Diamond(Map map, Position position, Sprite sprite) {
        super(map, position, sprite, speed);
    }

    /**
     *
     * @param x The x position you want the character to be on
     * @param y The y position you want the character to be on
     */
    public Diamond(Map map, int x, int y, Sprite sprite) {
        super(map, x, y, sprite, speed);
    }

    /**
     * if the element can fall
     * @return boolean
     */
    @Override
    public boolean canFall()
    {
        Mappable support = getMap().getNextElement(this, Direction.BOTTOM);

        System.out.println("Support: " + support);

        if(support == null)
            return true;

        if(!support.isAlive())
            return true;

        if(!( support instanceof Solid) && (!( support instanceof Entity) || isMoving()))
            return true;
        else if(support instanceof Movable)
            if(((Movable) support).isMoving())
                return true;

        return false;
    }

    /**
     * Makes the object fall
     */
    @Override
    public void fall()
    {
        move(Direction.BOTTOM);
    }

    /**
     * Remove the object from the map
     */
    @Override
    public void remove()
    {
        this.setAlive(false);
    }
}
