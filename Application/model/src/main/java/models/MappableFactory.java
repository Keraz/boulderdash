package models;

import lib.graphics.Direction;
import lib.graphics.Position;
import lib.graphics.Sprite;
import models.interfaces.Attractable;
import models.superclasses.Mappable;
import models.superclasses.Movable;
import org.w3c.dom.Attr;

import java.lang.reflect.InvocationTargetException;

public class MappableFactory {
    /**
     *
     * @param map map
     * @param name name
     * @param x x
     * @param y y
     * @param sprite sprite
     * @return element
     */
    public Mappable createModel(Map map, String name, int x, int y, Sprite sprite)
    {
        try
        {
            ModelType type = ModelType.getType(name);
            Mappable element = type.getClazz().getConstructor(Map.class, Position.class, Sprite.class).newInstance(map, new Position(x, y), sprite);
            element.addListener(map);

            return element;
        }
        catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     *
     * @param map map
     * @param type type
     * @param x x
     * @param y y
     * @param sprite sprite
     * @return element
     */
    public Mappable createModel(Map map, Class<? extends Mappable> type, int x, int y, Sprite sprite)
    {
        try
        {
            Mappable element = type.getConstructor(Map.class, Position.class, Sprite.class).newInstance(map, new Position(x, y), sprite);
            element.addListener(map);

            return element;
        }
            catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e)
        {
            e.printStackTrace();
        }

        return null;
    }

}
