package models;

import lib.graphics.Position;
import lib.graphics.Sprite;
import models.superclasses.Entity;

public class Character extends Entity {

    private final static int speed = 5;

    /**
     *
     * @param position The position you want the character to be on
     */
    public Character(Map map, Position position, Sprite sprite) {
        super(map, position, sprite, speed);
    }

    /**
     *
     * @param x The x position you want the character to be on
     * @param y The y position you want the character to be on
     */
    public Character(Map map, int x, int y, Sprite sprite) {
        super(map, x, y, sprite, speed);
    }
}
