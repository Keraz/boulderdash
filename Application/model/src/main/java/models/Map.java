package models;

import contracts.ModelInterface;
import listeners.EndingMoveEvent;
import listeners.MoveEvent;
import listeners.StartingMoveEvent;
import lib.graphics.Direction;
import lib.graphics.Position;
import lib.graphics.Sprite;
import lib.observer.Observable;
import listeners.MoveListener;
import models.interfaces.Attractable;
import models.interfaces.Breakable;
import models.interfaces.Destroyable;
import models.interfaces.Pushable;
import models.superclasses.Mappable;
import models.superclasses.Movable;

import java.util.TreeMap;

public class Map extends Observable implements ModelInterface, MoveListener {

    private int id;
    private TreeMap<Position, Mappable> map;
    private Character player;
    private int time, width, height;
    private int diamondsLeft;

    /**
     *
     * @param id id
     */
    public Map(int id)
    {
        this.map = new TreeMap<>();
        this.width = 0;
        this.height = 0;
        this.time = 0;
        this.id = id;
        diamondsLeft = 0;
    }

    /**
     * check the element
     */
    public void init()
    {
        map.forEach((position, element) ->
        {
            if(element instanceof Diamond)
                diamondsLeft++;

            if(element instanceof Attractable)
            {
                Attractable attractable = (Attractable) element;
                if(attractable.canFall())
                    attractable.fall();
            }
            if(element instanceof Enemy)
            {
                Direction enemyDirection = ((Enemy) element).findPath();
                if(enemyDirection != null)
                {
                    ((Enemy)element).move(enemyDirection);
                }
            }
        });
    }

    /**
     * action of the elmeent while moving
     * @param event event
     */
    @Override
    public void onMove(MoveEvent event)
    {
        if(player.isAlive() && diamondsLeft > 0)
        {
            Mappable targetElement = event.getTargetElement();
            Movable  sourceElement = event.getSourceElement();
            Position targetPosition = event.getTargetPosition();
            Position sourcePosition = event.getSourcePosition();

            if(targetElement != null)
            {

                if(sourceElement.isColliding(targetElement) && targetElement.isAlive() && sourceElement.isAlive())
                {
                    if(targetElement instanceof Diamond  && sourceElement instanceof Character)
                    {
                        diamondsLeft--;

                        ((Destroyable) targetElement).remove();
                        notifyObservers(targetElement);
                        removeElement(targetElement);
                    }

                    if(targetElement instanceof Enemy && sourceElement instanceof Character)
                    {
                        ((Destroyable) sourceElement).remove();
                        notifyObservers(sourceElement);
                        removeElement(sourceElement.getGridPosition());

                        return;
                    }

                    if(targetElement instanceof Destroyable && targetElement.getClass() != sourceElement.getClass())
                    {
                        if(sourceElement instanceof Enemy && !(targetElement instanceof Character))
                            return;

                        if(targetElement instanceof Character)
                        {
                            ((Destroyable) targetElement).remove();
                            notifyObservers(targetElement);
                            removeElement(targetElement.getGridPosition());
                            return;
                        }

                        if(!(sourceElement instanceof Character) || targetElement instanceof Breakable)
                        {
                            ((Destroyable) targetElement).remove();
                            System.out.println(targetElement.isAlive());
                            notifyObservers(targetElement);
                            removeElement(targetElement);

                            if(targetElement instanceof Enemy && sourceElement instanceof Destroyable)
                            {
                                ((Destroyable) sourceElement).remove();
                                notifyObservers(sourceElement);
                                removeElement(sourceElement);

                                Position diamondPosition = new Position(sourcePosition.getX(), sourcePosition.getY());

                                Diamond diamond = new Diamond(this, diamondPosition, new Sprite("diamond.png"));
                                map.put(diamondPosition, diamond);
                                notifyObservers(diamond);
                                if(diamond.canFall())
                                    diamond.fall();
                            }
                        }
                    }
                }
            }

            notifyObservers(event.getSourceElement());

        }
    }

    /**
     *  launch move
     * @param event event
     */
    @Override
    public void onStartingMove(StartingMoveEvent event)
    {
        if(player.isAlive() && diamondsLeft > 0)
        {
            Mappable targetElement = event.getTargetElement();
            Movable  sourceElement = event.getSourceElement();
            Direction direction    = event.getDirection();

            if(sourceElement instanceof Character)
            {
                if (targetElement instanceof Pushable
                        && targetElement.isAlive()
                        && direction != Direction.BOTTOM
                        && direction != Direction.TOP)
                {
                    ((Pushable) targetElement).push(direction);
                }
            }
        }
    }

    /**
     * End the move
     * @param event event
     */
    @Override
    public void onEndingMove(EndingMoveEvent event)
    {
        System.out.println(event.getSourceElement());
        if(player.isAlive() && diamondsLeft > 0)
        {
            Movable  sourceElement = event.getSourceElement();
            Position source = event.getSourcePosition();
            Direction direction    = event.getDirection();

            if(sourceElement instanceof Rock)
            {
                if(getNextElement(sourceElement, Direction.BOTTOM) instanceof Rock)
                {
                    Direction[] directions = {Direction.LEFT, Direction.RIGHT};

                    for (Direction currDirection : directions)
                    {
                        Mappable elementToAnalyse = getNextElement(getNextElement(sourceElement, currDirection), Direction.BOTTOM);

                        if(((Rock)sourceElement).canFallOnBlock(elementToAnalyse))
                        {
                            if(((Rock)sourceElement).canMove(currDirection))
                            {
                                ((Rock)sourceElement).move(currDirection);
                                break;
                            }
                        }

                    }
                }
            }

            if(sourceElement instanceof Attractable && sourceElement.isAlive())
            {
                System.out.println(sourceElement);
                Attractable attractable = (Attractable) sourceElement;
                if(attractable.canFall())
                    attractable.fall();
            }

            if(sourceElement instanceof Enemy && sourceElement.isAlive())
            {
                Direction enemyDirection = ((Enemy) sourceElement).findPath();
                if(enemyDirection != null)
                    sourceElement.move(enemyDirection);
            }

            Mappable aboveElement = getElement
            (
                new Position
                (
                source.getX(),
                source.getY() - 1
                )
            );

            if(aboveElement != null)
                if(aboveElement instanceof Attractable && aboveElement.isAlive())
                    if(((Attractable) aboveElement).canFall())
                        ((Attractable) aboveElement).fall();
        }
    }

    /**
     * check the next element
     * @param element element
     * @param direction direction
     * @return element
     */
    public Mappable getNextElement(Mappable element,Direction direction)
    {
        if(element == null)
            return null;

        return getElement(
                new Position
                        (
                                element.getGridPosition().getX() + direction.getxIncrement(),
                                element.getGridPosition().getY() + direction.getyIncrement()
                        )
        );
    }

    /**
     *
     * @param position The position of the element you want to get
     * @return The element at the position given
     */
    public Mappable getElement(Position position)
    {
        return this.map.get(position);
    }

    /**
     *
     * @param position The position you want the element to be on
     * @param element The element you want to place on the map
     */
    public void putElement(Position position, Mappable element)
    {
        this.map.put(position, element);
    }

    /**
     *
     * @param position The position of the element you want to remove
     */
    public void removeElement(Position position)
    {
        this.map.remove(position);
    }

    /**
     *
     * @param mappable mappable
     */
    public void removeElement(Mappable mappable)
    {
        this.map.remove(mappable.getGridPosition(), mappable);
    }

    /**
     *
     * @return map
     */
    public TreeMap<Position, Mappable> getMap() {
        return map;
    }

    /**
     *
     * @param map map
     */
    public void setMap(TreeMap<Position, Mappable> map) {
        this.map = map;
    }

    /**
     *
     * @return time
     */
    public int getTime() {
        return time;
    }

    /**
     *
     * @param time time
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     *
     * @param width width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     *
     * @return Height
     */
    public int getHeight() {
        return height;
    }

    /**
     *
     * @param height height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     *
     * @return Player
     */
    public Character getPlayer() {
        return player;
    }

    /**
     *
     * @param player player
     */
    public void setPlayer(Character player) {
        this.player = player;
    }

    /**
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return DiamondsLeft
     */
    public int getDiamondsLeft() {
        return diamondsLeft;
    }

    /**
     *
     * @param diamondsLeft diamondsLeft
     */
    public void setDiamondsLeft(int diamondsLeft) {
        this.diamondsLeft = diamondsLeft;
    }
}
