package models;

import models.superclasses.Mappable;

public enum ModelType {

    CHARACTER(4, Character.class),
    DIAMOND(1, Diamond.class),
    DIRT(5, Dirt.class),
    ENEMY(3, Enemy.class),
    ROCK(2, Rock.class),
    WALL(6, Wall.class);

    private Class<? extends Mappable> clazz;
    private int id;

    /**
     *
     * @param id id
     * @param clazz class
     */
    ModelType(int id, Class<? extends Mappable> clazz)
    {
        this.clazz = clazz;
        this.id = id;
    }

    /**
     *
     * @param name name
     * @return type
     */
    public static ModelType getType(String name)
    {
        for (ModelType type : values())
        {
            if(name.toLowerCase().equals(type.name().toLowerCase()))
            {
                return type;
            }
        }

        return null;
    }

    /**
     *
     * @return id
     */
    public int getId()
    {
        return id;
    }

    /**
     *
     * @param id id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     *
     * @return Class
     */
    public Class<? extends Mappable> getClazz() {
        return clazz;
    }

    /**
     *
     * @param clazz clazz
     */
    public void setClazz(Class<? extends Mappable> clazz) {
        this.clazz = clazz;
    }

}
