package models.superclasses;

import lib.graphics.Position;
import models.Character;
import models.Dirt;
import models.Map;
import models.Rock;
import org.junit.Before;

import static org.junit.Assert.*;

public class MappableTest {

    private Mappable mappable;

    @Before
    public void setUp() throws Exception
    {
        this.mappable = new Character(new Map(1), 1, 1, null);
    }

    /**
     * Test if is colliding returns a correct value
     */
    @org.junit.Test
    public void isColliding()
    {
        Mappable dirt = new Dirt(new Map(1), 0, 0, null);
        assert !mappable.isColliding(dirt);
        Mappable stone = new Rock(new Map(1), 1, 1, null);
        stone.setAbsolutePosition(new Position(60, 60));
        assert mappable.isColliding(stone);
    }
}