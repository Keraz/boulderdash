package lib.graphics;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Sprite extends JPanel  {

    /**
     * The image shown by the sprite
     */
    private Image image;

    /**
     *
     * @param path The path to the image shown by the sprite
     */
    public Sprite(String path)
    {
        try
        {
            this.image = ImageIO.read(Sprite.class.getClassLoader().getResourceAsStream(path));
            this.setPreferredSize(new Dimension(64, 64));
            this.setMinimumSize(new Dimension(64, 64));
            this.setBackground(new Color(0, 0, 0, 0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param image The image shown by the sprite
     */
    public Sprite(Image image)
    {
        this.image = image;
        this.setPreferredSize(new Dimension(64, 64));
        this.setMinimumSize(new Dimension(64, 64));
    }

    /**
     *
     * @return The current image show by the sprite
     */
    public Image getImage()
    {
        return image;
    }

    /**
     *
     * @param image The image show by the sprite
     */
    public void setImage(Image image)
    {
        this.image = image;
    }

    /**
     * Enables to show the sprite
     * @param g The graphics object that enable to draw the image
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
    }
}
