package lib.graphics;

public enum Direction {

    TOP( 0, -1),
    RIGHT(1, 0),
    BOTTOM(0, 1),
    LEFT(-1, 0);

    /**
     * The number of blocks an entity might travel in X axis while heading toward this direction
     */
    int xIncrement;

    /**
     * The number of blocks an entity might travel in Y axis while heading toward this direction
     */
    int yIncrement;

    /**
     *
     * @param xIncrement The number of blocks an entity might travel in X axis while heading toward this direction
     * @param yIncrement The number of blocks an entity might travel in Y axis while heading toward this direction
     */
    Direction(int xIncrement, int yIncrement)
    {
        this.xIncrement = xIncrement;
        this.yIncrement = yIncrement;
    }

    /**
     * Enables to get the number of blocks an entity might travel in X axis while heading toward this direction
     * @return The number of blocks an entity might travel in X axis while heading toward this direction
     */
    public int getxIncrement() {
        return xIncrement;
    }

    /**
     * Enables to get the number of blocks an entity might travel in Y axis while heading toward this direction
     * @return The number of blocks an entity might travel in Y axis while heading toward this direction
     */
    public int getyIncrement() {
        return yIncrement;
    }
}
