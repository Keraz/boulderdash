package lib.graphics;

public class Position implements Comparable<Position> {

    /**
     * The position in the X axis
     */
    private int x;

    /**
     * The position in the Y axis
     */
    private int y;

    /**
     *
     * @param x The position in the X axis
     * @param y The position in the Y axis
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @return The position in the X axis
     */
    public int getX() {
        return x;
    }

    /**
     *
     * @return Y The position in the Y axis
     */
    public int getY() {
        return y;
    }

    /**
     *
     * @param x The position in the X axis
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     *
     * @param y The position in the Y axis
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Enables to compare two positions
     * @param position The position to compare with the current position
     * @return 1 If the given position is greater than the current one, 0 if it is equal and -1 if it is lower
     */
    @Override
    public int compareTo(Position position)
    {
        if(position.getY() == this.y)
            if(position.getX() == this.x)
                return 0;
            else
                return position.getX() > this.x ? 1 : -1;
        else
            return position.getY() > this.y ? 1 : -1;
    }

    /**
     * Enables to know if the given object is equal to this one
     * @param obj The object you want to compare
     * @return if the given object is equal to the current one
     */
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Position)
        {
            Position position = (Position) obj;

            return position.getX() == x && position.getY() == y;
        }

        return super.equals(obj);
    }

    public String toString()
    {
        return "x: " + x + ", y = " + y + ";";
    }
}
