package lib.graphics.animation;

import lib.graphics.Sprite;
import lib.observer.Observable;

import java.util.ArrayList;

public class Animation extends Observable {

    /**
     * A collection of sprites
     */
    private ArrayList<Sprite> sprites;

    public Animation(ArrayList<Sprite> sprites)
    {
        this.sprites = sprites;
    }

    /**
     *
     * @param index The index of the sprite you want in the collection
     * @return The sprite located in the collection at the given index
     */
    public Sprite getSprite(int index)
    {
        return this.sprites.get(index);
    }

    /**
     *
     * @return The entire sprites collection
     */
    public ArrayList<Sprite> getSprites() {
        return sprites;
    }

}
