package lib.graphics.animation;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class AnimationManager {

    /**
     * The singleton instance of the manager
     */
    private static AnimationManager instance = null;

    /**
     * The XML document read by the manager
     */
    private Document document;

    /**
     * The animations referenced in the document read by the manager
     */
    private HashMap<String, Animation> animations;

    /**
     *
     * @param file The XML file which references all the animations
     */
    private AnimationManager(File file)
    {
        try
        {
            this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
        }
        catch(IOException | SAXException | ParserConfigurationException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new instance of the manager
     * @param file The XML file which references all the animations
     * @return A new instance of the AnimationManager
     */
    public static AnimationManager createInstance(File file)
    {
        instance = new AnimationManager(file);
        return instance;
    }

    /**
     * Enables to get the current instance of the manager
     * @return An existing instance of the AnimationManager
     */
    public static AnimationManager getInstance()
    {
        return instance;
    }

    /**
     * Permit to read the XML document which references all the animations in order to store those animations into a collection
     */
    private void read()
    {
        //TODO : NOT IMPLEMENTED YET
    }

    /**
     *
     * @param name The name of the animation you are looking for
     * @return The animation whose name is the one you gave
     */
    public Animation getAnimation(String name)
    {
        return this.animations.get(name);
    }

}
