package lib.graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;

public class Window extends JFrame {

    /**
     * The singleton instance of the window
     */
    private static Window instance = new Window();

    /**
     * The main panel of the window
     */
    private JPanel mainPanel;

    private Window() throws HeadlessException {

        this.setTitle("Boulder Dash");
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setFocusable(true);

        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(null);
        this.add(this.mainPanel);
        this.pack();

        this.setVisible(true);

    }

    /**
     * Enables to remove all the key listeners
     */
    public void removeAllKeyListeners()
    {
        for (KeyListener keyListener : getKeyListeners())
            this.removeKeyListener(keyListener);
    }

    /**
     *
     * @return The main panel of the window
     */
    public JPanel getMainPanel() {
        return mainPanel;
    }

    /**
     *
     * @param mainPanel The main panel of the window
     */
    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    /**
     *
     * @return The current instance of the window
     */
    public static Window getInstance()
    {
        return instance;
    }
}
