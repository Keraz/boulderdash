package lib.observer;

public interface Observer {

    /**
     * @param object The object which has been sent by an observable
     */
    public void update(Object object);

}
