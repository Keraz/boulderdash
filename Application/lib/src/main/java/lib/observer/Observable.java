package lib.observer;

import java.util.ArrayList;

public abstract class Observable {

    /**
     * The list of observers that are observing the current observable object
     */
    private ArrayList<Observer> observers;

    public Observable()
    {
        this.observers = new ArrayList<>();
    }

    /**
     *
     * @param observer The observer to subscribe to this entity
     */
    public void addObserver(Observer observer)
    {
        this.observers.add(observer);
    }

    /**
     *
     * @param observer The observer to unsubscribe from this entity
     */
    public void removeObserver (Observer observer)
    {
        this.observers.remove(observer);
    }

    /**
     * Enables to notify all the observers
     * @param object An object to send to the observers that are observing this entity
     */
    public void notifyObservers(Object object)
    {
        observers.forEach(observer -> observer.update(object));
    }

    public ArrayList<Observer> getObservers() {
        return observers;
    }
}
