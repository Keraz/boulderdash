package routing;

import lib.graphics.Direction;

public enum Request
{

    MOVE_TOP(Direction.TOP),
    MOVE_RIGHT(Direction.RIGHT),
    MOVE_BOTTOM(Direction.BOTTOM),
    MOVE_LEFT(Direction.LEFT),
    CHOOSE_GAME(1),
    CHOOSE_EDITOR(0),
    LAUNCH_GAME(null),
    LAUNCH_EDITOR(null),
    SAVE_LEVEL(null),
    MAIN_MENU(null),
    GAME_VICTORY(null),
    GAME_DEFEAT(null);

    private Object argument;

    /**
     *
     * @param argument argument
     */
    Request(Object argument) {
        this.argument = argument;
    }

    public Object getArgument() {
        return argument;
    }

    /**
     *
     * @param name type
     * @return request
     */
    public static Request getRequest(String name)
    {
        for (Request request : values())
            if(name.equals(request.name()))
                return request;

        return null;
    }

    /**
     *
     * @param argument argument
     */
    public void setArgument(Object argument)
    {
        this.argument = argument;
    }
}
