package routing;

import contracts.ControllerInterface;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Route {

    private ControllerInterface controller;
    private Method action;
    private Request request;

    /**
     *
     * @param controller controller
     * @param method method
     * @param request request
     */
    public Route(ControllerInterface controller, Method method, Request request) {
        this.controller = controller;
        this.action = method;
        this.request = request;
    }

    /**
     * call the action to do
     */
    public void callAction()
    {
        try
        {
            action.invoke(controller, request);
        }
        catch(IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
            System.err.println(e.getCause());
        }
    }

    /**
     *
     * @return controller
     */
    public ControllerInterface getController() {
        return controller;
    }

    /**
     *
     * @param controller controller
     */
    public void setController(ControllerInterface controller) {
        this.controller = controller;
    }

    /**
     *
     * @return action
     */
    public Method getAction() {
        return action;
    }

    /**
     *
     * @param action action
     */
    public void setAction(Method action) {
        this.action = action;
    }

    /**
     *
     * @return request
     */
    public Request getRequest() {
        return request;
    }

    /**
     *
     * @param request reuest
     */
    public void setRequest(Request request) {
        this.request = request;
    }
}
