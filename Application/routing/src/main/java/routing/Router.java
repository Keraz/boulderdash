package routing;

import contracts.ControllerInterface;
import contracts.ViewInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class Router {

    private static Router instance = null;
    private static HashMap<Class<? extends ControllerInterface>, ControllerInterface> controllers = new HashMap<>();

    private Class<? extends ViewInterface> view;
    private Document document;
    private HashMap<Integer, Route> routes;

    /**
     *
     * @param view view
     */
    private Router(Class<? extends ViewInterface> view)
    {
        try
        {
            InputStream file = Router.class.getClassLoader().getResourceAsStream("router.xml");
            this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
            this.view = view;
            this.routes = new HashMap<>();
            loadRoutes();
        }
        catch(ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param view view
     * @return instance
     */
    public static Router createInstance(Class<? extends ViewInterface> view)
    {
        instance = new Router(view);
        return instance;
    }

    /**
     *
     * @return instance
     */
    public static Router getInstance()
    {
        return instance;
    }

    /**
     * load the routes
     */
    public void loadRoutes()
    {
        try
        {
            NodeList routersList = document.getElementsByTagName("Router");
            for (int i = 0; i < routersList.getLength(); i++) {
                Element routerElement = (Element) routersList.item(i);
                String stringView = routerElement.getAttribute("view");

                if(stringView.equals(view.getSimpleName()))
                {
                    NodeList routesList = routerElement.getElementsByTagName("Route");
                    for(int j = 0; j < routesList.getLength(); j++)
                    {
                        Element element = (Element) routesList.item(j);
                        String sController = element.getAttribute("controller");
                        String sAction = element.getAttribute("action");
                        String sRequest = element.getAttribute("name");
                        Request request = Request.getRequest(sRequest);

                        Class<? extends ControllerInterface> controllerClazz = (Class<? extends ControllerInterface>) Class.forName("controllers." + sController + "Controller");

                        ControllerInterface controller = null;
                        if(controllers.containsKey(controllerClazz))
                        {
                            controller = controllers.get(controllerClazz);
                        }
                        else
                        {
                            controller = (ControllerInterface) controllerClazz.getConstructor().newInstance();
                            controllers.put(controllerClazz, controller);
                        }



                        Method action = controller.getClass().getDeclaredMethod("launch" + sAction, Request.class);

                        routes.put(Integer.parseInt(element.getAttribute("input")), new Route(controller, action, request));
                    }
                }
            }
        }
        catch(ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param input input
     * @return boolean
     */
    public boolean hasRoute(int input)
    {
        return routes.containsKey(input);
    }

    /**
     *
     * @param input input
     */
    public void callRoute(int input)
    {
        if(hasRoute(input))
        {
            routes.get(input).callAction();
        }
    }

    /**
     *
     * @param input input
     * @param argument argument
     */
    public void callRoute(int input, Object argument)
    {
        if(hasRoute(input))
        {
            Route route = routes.get(input);
            route.getRequest().setArgument(argument);
            route.callAction();
        }
    }

    /**
     *
     * @param clazz class
     * @param controller controller
     */
    public static void addController(Class<? extends ControllerInterface> clazz, ControllerInterface controller)
    {
        controllers.put(clazz, controller);
    }

}
