package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlDao {

    /**
     * The Singleton current instance of the DAO
     */
    private static SqlDao instance = null;

    /**
     * The domain name of the server hosting the database
     */
    private static String host = "127.0.0.1";

    /**
     * The port used by SQL on this server
     */
    private static String port = "3306";

    /**
     * The name of the database
     */
    private static String dbname = "boulder_dash";

    /**
     * The username that permit to access to the database
     */
    private static String user = "root";

    /**
     * The password that permit to access to the database
     */
    private static String password = "";

    /**
     * The connection object to the database
     */
    private Connection connection;

    private SqlDao()
    {
        try
        {
            this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbname, user, password);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new instance of the DAO by initializing a new connection to the database
     * @return A new instance of the DAO
     */
    public static SqlDao createInstance()
    {
        instance = new SqlDao();
        return instance;
    }

    /**
     * Enables to get the current instance of the DAO
     * @return The current instance of the DAO
     */
    public static SqlDao getInstance()
    {
        return instance;
    }

    /**
     * Enables to get the connection object to the database
     * @return The SQL connection
     */
    public Connection getConnection()
    {
        return connection;
    }

}
